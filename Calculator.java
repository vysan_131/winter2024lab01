import java.util.Random;
public class Calculator{
	
	public static int sum(int x, int y){
		return x + y;
	}	
	public static double squareRoot(int x){
		return Math.sqrt(x);
	}
	public static int random(){
		Random random = new Random();
		int x = random.nextInt(500000);
		return x;
	}	
	public static int divide(int x, int y){
		if(y > 0){
			return x/y;
		}
		else{
			return 0;
		}	
	}
}	
	
		